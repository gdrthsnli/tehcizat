USE [master]
GO
/****** Object:  Database [tehcizat]    Script Date: 02.04.2015 03:38:17 ******/
CREATE DATABASE [tehcizat]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'tehcizat', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\tehcizat.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'tehcizat_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\tehcizat_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [tehcizat] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [tehcizat].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [tehcizat] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [tehcizat] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [tehcizat] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [tehcizat] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [tehcizat] SET ARITHABORT OFF 
GO
ALTER DATABASE [tehcizat] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [tehcizat] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [tehcizat] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [tehcizat] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [tehcizat] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [tehcizat] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [tehcizat] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [tehcizat] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [tehcizat] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [tehcizat] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [tehcizat] SET  DISABLE_BROKER 
GO
ALTER DATABASE [tehcizat] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [tehcizat] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [tehcizat] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [tehcizat] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [tehcizat] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [tehcizat] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [tehcizat] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [tehcizat] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [tehcizat] SET  MULTI_USER 
GO
ALTER DATABASE [tehcizat] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [tehcizat] SET DB_CHAINING OFF 
GO
ALTER DATABASE [tehcizat] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [tehcizat] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [tehcizat]
GO
/****** Object:  Table [dbo].[company]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[company](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[address] [text] NULL,
 CONSTRAINT [PK_company] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[currency]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[currency](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_currency] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[history]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[history](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NULL,
	[type_id] [int] NULL,
	[company_suplier_id] [int] NULL,
	[company_orderer_id] [int] NULL,
	[product_id] [int] NULL,
	[price] [float] NULL,
	[currency_id] [int] NULL,
	[amount] [float] NULL,
	[unit_id] [int] NULL,
	[orderer_id] [int] NULL,
	[date] [text] NULL,
 CONSTRAINT [PK_history] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[item_prices]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item_prices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_id] [int] NULL,
	[price] [float] NULL,
	[currency_id] [int] NULL,
	[company_id] [int] NULL,
	[date] [text] NULL,
 CONSTRAINT [PK_item_prices] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[orderer]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orderer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[surname] [nvarchar](50) NULL,
	[company_id] [int] NOT NULL,
 CONSTRAINT [PK_orderer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[price_list]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[price_list](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[request_id] [int] NULL,
	[new_or_old] [nvarchar](50) NULL,
	[company_id] [int] NULL,
	[price] [int] NULL,
	[currency_id] [int] NULL,
 CONSTRAINT [PK_price_list] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[product]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[request]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[request](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[amount] [float] NULL,
	[product_id] [int] NULL,
	[unit_id] [int] NULL,
	[source_id] [int] NULL,
	[type_id] [int] NULL,
	[location] [nvarchar](50) NULL,
	[company_id] [int] NULL,
	[status] [int] NULL,
	[orderer_id] [int] NOT NULL,
 CONSTRAINT [PK_request] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[request_source]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[request_source](
	[id_source] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_request_source] PRIMARY KEY CLUSTERED 
(
	[id_source] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[request_type]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[request_type](
	[id_type] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_request_type] PRIMARY KEY CLUSTERED 
(
	[id_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[source]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[source](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_source] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[type]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[unit]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[unit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_unit] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user]    Script Date: 02.04.2015 03:38:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[password] [nvarchar](100) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NULL,
	[surname] [nvarchar](50) NULL,
	[role] [int] NOT NULL,
	[count] [int] NOT NULL,
	[phone] [nvarchar](50) NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[user] ON 

INSERT [dbo].[user] ([id], [username], [password], [email], [name], [surname], [role], [count], [phone]) VALUES (3, N'gdrt94', N'0505545453', N'gudrat94@gmail.com', NULL, NULL, 1, 0, N'')
INSERT [dbo].[user] ([id], [username], [password], [email], [name], [surname], [role], [count], [phone]) VALUES (6, N'alvinazer', N'0517504043', N'elvin.akhindzadeh@gmail.com', NULL, NULL, 2, 0, NULL)
INSERT [dbo].[user] ([id], [username], [password], [email], [name], [surname], [role], [count], [phone]) VALUES (7, N'fariz', N'0517788594', N'fhuseynli@yahoo.com', NULL, NULL, 3, 0, NULL)
SET IDENTITY_INSERT [dbo].[user] OFF
USE [master]
GO
ALTER DATABASE [tehcizat] SET  READ_WRITE 
GO
